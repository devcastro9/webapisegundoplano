﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace WebApiSegundoPlano.HostedServices
{
    public class WorkerSegundoPlano : IHostedService, IDisposable
    {
        private readonly ILogger<WorkerSegundoPlano> _logger;
        private Timer? _timer;
        public WorkerSegundoPlano(ILogger<WorkerSegundoPlano> logger)
        {
            _logger = logger;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted service programado iniciando.");
            _timer = new Timer(GuardarArchivo, null, TimeSpan.Zero, TimeSpan.FromSeconds(5));
            return Task.CompletedTask;
        }
        private void GuardarArchivo(object? state)
        {
            string namefile = "Archivo_" + DateTime.Now.ToString("dd-hh-mm-ss") + ".txt";
            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, namefile);
            File.Create(path);
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Hosted Service programado deteniendose.");
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
